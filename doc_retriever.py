#  * Copyright(C) Biorelate Ltd. - All Rights Reserved
#  * Unauthorized copying of this file, via any medium is strictly prohibited.
#  * Proprietary and confidential.
import argparse
import os
from google.cloud import datastore
import gzip
import json
import base64
import copy

parser = argparse.ArgumentParser()
parser.add_argument('doc_count', help="The ")
parser.add_argument('output_path', help="The directory to write documents to")
args = parser.parse_args()
doc_count = int(args.doc_count)


def calculate_id(document):
    json_document = json.loads(document)
    ids = copy.deepcopy(json_document["id"])
    ids.sort()
    id_string = "&".join(ids)
    return base64.urlsafe_b64encode(bytes(id_string, "UTF-8")).decode("UTF-8")


client = datastore.Client()
doc_entries = list(client.query(kind='Publication').fetch(doc_count))
skip_count = 0
for doc_entry in doc_entries:
    decompressed_doc = gzip.decompress(doc_entry['compressedJSON'])
    doc_id = str(calculate_id(decompressed_doc))
    # if len(doc_id) > 100:
    #     doc_id = doc_id[:99]
    try:
        output_file = open(os.path.join(args.output_path, doc_id + ".json"), "wb")
        output_file.write(decompressed_doc)
        output_file.close()
    except OSError:
        print("Skipping file:", doc_id)
        skip_count += 1

print("Skipped:", skip_count)
